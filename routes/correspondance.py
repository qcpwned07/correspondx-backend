from flask import Blueprint, session, jsonify, request
from datetime import datetime
import uuid
import hashlib
from db.models import Correspondance, db
from db.schemas import CorrespondanceSchema, ma


# Creer le blueprint correspondance (importer ensuite avec from correspondance import correspondance)
correspondance = Blueprint('blueprint', __name__,)
correspondance_schema = CorrespondanceSchema(strict=True)
correspondances_schema = CorrespondanceSchema(many=True, strict=True)


@correspondance.route("/test")
def testing_route():
    return jsonify("HelloWorlds")


@correspondance.route("/correspondance", methods=['GET'])
def correspondances_get():
    # Aller chercher dans DB
    all_correspondances = Correspondance.query.all()
    # Mettre dans schema
    result = correspondances_schema.dump(all_correspondances)
    return jsonify(result)


@correspondance.route("/correspondance/<id>", methods=['GET'])
def correspondance_get(id):
    # Aller chercher dans DB
    corresp = Correspondance.query.get(id)
    return correspondance_schema.jsonify(corresp)

# TODO Changer les param de user a correspondance
@correspondance.route("/correspondance", methods=['POST'])
def correspondance_post():
    #name = request.json
    correspondancename = request.form['correspondancename']
    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    password = request.form['password']

    date_joined = datetime.now()
    # Creer le salt
    salt = uuid.uuid4().hex
    # Hasher le password
    #password = hashlib.sha512(str(correspondance["password"] + salt)
    #                                 .encode("utf-8")).hexdigest()
    # Creer l'utilisateur
    new_correspondance = Correspondance(correspondancename, email, salt, password, firstname, lastname)
    # Ajouter dans la base de donnees
    db.session.add(new_correspondance)
    db.session.commit()

    return CorrespondanceSchema.jsonify("Helloworld")

@correspondance.route("/correspondance", methods=['PUT'])
def correspondance_put():
    return None

@correspondance.route("/correspondance", methods=['DELETE'])
def correspondance_delete():
    return None