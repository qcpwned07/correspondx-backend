from flask import Blueprint, session, jsonify, request
from datetime import datetime
import uuid
import hashlib
from db.models import Utilisateur, db
from db.schemas import UserSchema, ma


# Creer le blueprint user (importer ensuite avec from user import user)
user = Blueprint('blueprint', __name__,)
user_schema = UserSchema(strict=True)
users_schema = UserSchema(many=True, strict=True)

@user.route("/test")
def testing_route():
    return jsonify("HelloWorlds")

@user.route("/user", methods=['GET'])
def users_get():
    # Aller chercher dans DB
    all_users = Utilisateur.query.all()
    # Mettre dans schema
    result = users_schema.dump(all_users)
    return jsonify(result)


@user.route("/user/<id>", methods=['GET'])
def user_get(id):
    # Aller chercher dans DB
    utilisateur = Utilisateur.query.get(id)
    return user_schema.jsonify(utilisateur)


@user.route("/user", methods=['POST'])
def user_post():
    #name = request.json
    username = request.form['username']
    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    password = request.form['password']

    date_joined = datetime.now()
    # Creer le salt
    salt = uuid.uuid4().hex
    # Hasher le password
    #password = hashlib.sha512(str(user["password"] + salt)
    #                                 .encode("utf-8")).hexdigest()
    # Creer l'utilisateur
    new_user = Utilisateur(username, email, salt, password, firstname, lastname)
    # Ajouter dans la base de donnees
    db.session.add(new_user)
    db.session.commit()

    return UserSchema.jsonify("Helloworld")

@user.route("/user", methods=['PUT'])
def user_put():
    return None

@user.route("/user", methods=['DELETE'])
def user_delete():
    return None
