#Import librairies
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
# Import the configuration files
from .config import app_config

# Import the models and schemas
from db.models import Utilisateur, Correspondance, db
from db.schemas import UserSchema, CorrespondanceSchema, ma

# Import the different routes
from routes.correspondance import *
from routes.user import user
from src import app

def create_app(env_name):
    """
    Create app
    """

    # App initiliazation
    # app = Flask(__name__)

    # Use the config from config.py
    app.config.from_object(app_config[env_name])

    # Init db
    # db = SQLAlchemy(app)
    # Init ma
    # ma = Marshmallow(app)

    # Initializing the schema
    user_schema = UserSchema(strict=True)
    users_schema = UserSchema(many=True, strict=True)
    correspondance_schema = CorrespondanceSchema(strict=True)
    correspondances_schema = CorrespondanceSchema(many=True, strict=True)

    # Request route middleware (pour les routes externes a ce fichier)
    app.register_blueprint(user)





    @app.route('/', methods=['GET'])
    def index():
      """
      example endpoint
      """
      return 'Congratulations! Your first endpoint is workin'

    return app

