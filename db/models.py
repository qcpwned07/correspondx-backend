from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import os
import uuid
import hashlib
import sys
import os.path

# Sauvegarder le basedir, et le changer pour les imports
basedir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from src import app




"""
Preparation des variables
"""

#app = Flask(__name__)


"""
A CHANGER AVANT DE DEPLOYER EN PRODUCTION
"""
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'correspondx.db')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///book.sqlite'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root@localhost/book'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


"""
Creation des classes ORM
"""


class Utilisateur(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(25), unique=True)
    salt = db.Column(db.String)
    password = db.Column(db.String)
    email = db.Column(db.String(100))

    firstname = db.Column(db.String(100))
    lastname = db.Column(db.String(100))
    date_joined = db.Column(db.DateTime)

    def __init__(self, username, email, salt, password, firstname, lastname):
        self.username = username
        self.password = password
        self.email = email
        self.firstname = firstname
        self.lastname = lastname
        # Creer le salt
        self.salt =  salt #uuid.uuid4().hex
        # Hasher le password
        hashed_password = hashlib.sha512(str(password + salt)
                                         .encode("utf-8")).hexdigest()
        self.date_joined = datetime.now()

    def __repr__(self):
        return "<User(username='%s', firstname='%s', lastname='%s'>" % (
            self.username, self.firstname, self.lastname)


class Correspondance(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50), unique=False)
    description = db.Column(db.String(500), unique=False)
    date_created = db.Column(db.DateTime)
    author_a_id = db.Column(db.Integer, db.ForeignKey('utilisateur.id'))
    author_b_id = db.Column(db.Integer, db.ForeignKey('utilisateur.id'))


